
    document.addEventListener('DOMContentLoaded', function () {
        var tabButtons = document.querySelectorAll('.tabs_buttons .tab-button');
        var tabContents = document.querySelectorAll('.tabs-content .tab-content');
    
        // Make the first tab active by default
        if (tabButtons.length > 0 && tabContents.length > 0) {
            tabButtons[0].classList.add('active');
            tabContents[0].classList.add('active');
        }
    
        tabButtons.forEach(function (button, index) {
            button.addEventListener('click', function () {
                // Remove 'active' class from all buttons and content
                tabButtons.forEach(function (btn) {
                    btn.classList.remove('active');
                });
                tabContents.forEach(function (content) {
                    content.classList.remove('active');
                });
    
                // Add 'active' class to the clicked button and content
                this.classList.add('active');
                tabContents[index].classList.add('active');
            });
        });
    });
    
    

    //     var tabButtons = document.querySelectorAll('.tabs_buttons .tab-button');
    //     var tabContents = document.querySelectorAll('.tabs-content .tab-content');
    
    //     // Function to apply styles
    //     function applyStyles(element, isActive) {
    //         if (isActive) {
    //             element.style.backgroundColor = element.dataset.bgColorActive;
    //             element.style.color = element.dataset.textColorActive;
    //         } else {
    //             element.style.backgroundColor = element.dataset.bgColor;
    //             element.style.color = element.dataset.textColor;
    //         }
    //     }
    
    //     // Make the first tab active by default
    //     if (tabButtons.length > 0 && tabContents.length > 0) {
    //         tabButtons[0].classList.add('active');
    //         tabContents[0].classList.add('active');
    //         applyStyles(tabButtons[0], true); // Apply active styles to the first tab
    //     }
    
    //     tabButtons.forEach(function (button, index) {
    //         button.addEventListener('click', function () {
    //             // Remove 'active' class and apply non-active styles for all buttons
    //             tabButtons.forEach(function (btn) {
    //                 btn.classList.remove('active');
    //                 applyStyles(btn, false); // Apply non-active styles
    //             });
    
    //             // Add 'active' class and apply active styles to the clicked button
    //             this.classList.add('active');
    //             applyStyles(this, true); // Apply active styles
    
    //             // Add 'active' class to the corresponding tab content
    //             tabContents.forEach(function (content) {
    //                 content.classList.remove('active');
    //             });
    //             tabContents[index].classList.add('active');
    //         });
    //     });
    // });
    
    
    